package nl.politie.bvh.fitnesse;

import fitnesse.junit.FitNesseRunner;
import org.junit.runner.RunWith;

@RunWith(FitNesseRunner.class)
@FitNesseRunner.Suite(ProefTest.SUITE)
@FitNesseRunner.FitnesseDir(".")
@FitNesseRunner.OutputDir("./target/fitnesse-results")
public class ProefTest {
    public static final String SUITE="ProjectTemplate.TestProfielen.Tester01.HuidigeVersie.TestUnits.TestSuite01.TestCase01";

}
