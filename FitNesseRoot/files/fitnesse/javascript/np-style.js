/* custom styling voor Politie             */
/* november 2014, Edwin Roetman            */
/* style werkt niet 100% zonder dit script */
/* copied from fitness_straight.js         */
$(function () {

	/**
	 * Change a.button to a real button (so it looks the same), retaining link behaviour
	 */
	$('a.button').replaceWith(function () {
		var self = $(this);
		var button = $('<button/>');
		button.text(self.text());
		button.click(function () {
			window.location = self.attr('href');
			return false;
		});
		return button;
	});

});
window.onload = function() {
  /* Purge buttons op TestHistory pagina verbergen behalve Purge > 30 days */
   $(":contains('Purge')").closest('button').hide();
   $(":contains('Purge > 30 days')").closest('button').show();
};
