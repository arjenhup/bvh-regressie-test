;
; AutoIt Version: 3.3.8.1
; Author:         Erik Breuker, Edwin Roetman
;
; Script Function:
;   Inloggen op een webbrowser t.b.v. automatisch testen
;   Parameters:
;   1: username
;   2: password
;   3: window title (optioneel)
;      default: "Windows-beveiliging" werkt voor Internet Explorer
;      firefox:  "Authentication Required"
;      chrome :  Applicatie- en url-specifiek, bv. "bvh-bosz-ken-o.swh04.local - Google Chrome"

; Minimaal twee parameters: gebruikersnaam en wachtwoord
If ($CmdLine[0] < 2) Then
   Call("Fout", "Geen gebruikersnaam en wachtwoord opgegeven")
EndIf

Local $username = $CmdLine[1]
Local $password = $CmdLine[2]

; Window title zetten met parameter 3 of default
Local $windowtitle = "Windows-beveiliging" ;default tbv IE
If ($CmdLine[0] = 3) Then
   $windowtitle = $CmdLine[3]
EndIf

; Default timing instellingen
Opt("SendKeyDelay", 25)

; Zoeken van het window
Local $hWnd = WinWait($windowtitle,"", 5)
If $hWnd = 0 Then
   Call("Fout", "Window '" & $windowtitle & "' niet gevonden")
EndIf
WinActivate($hWnd)
If WinActive($hWnd) <> $hWnd Then
   Call("Fout", "Window '" & $windowtitle & "' niet geactiveerd")
EndIf   

; het echte inloggen
Send($username) 
Send("{Tab}") 
Send($password)
Send("{Enter}")

; Finished!
Exit(0)

; Foutmelding schrijven en stoppen met exitcode 1
Func Fout($bericht)
   ConsoleWriteError("FOUT: " & $bericht & @CRLF)
   Exit(1)  
EndFunc
