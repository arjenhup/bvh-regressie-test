'''Import Xebium fixture'''
|import|
|com.xebia.incubator.xebium |
|slim |

'''Activate Xebium fixture'''
| library |
| selenium driver fixture |

'''Database setup '''
!| script | Db Slim Setup |!-com.sybase.jdbc3.jdbc.SybDataSource-!| jdbc:sybase:Tds:${DB_DBCONNECT}/${DB_DBNAME} | ${DB_USER} | ${DB_PWD} |

'''Start de browser'''
| script |
| load custom browser preferences from file | ${BROWSER_SETTINGS} |
| set timeout to | 5 | seconds |
| start browser | ${BROWSER} | on url | http://${USER_NAME}:${USER_PASSWORD}@${URL_BVHWEB} |

'''Screenshots'''
!*****< Verborgen-!
|script|
| note | LET OP: de variabele $dag_nummer wordt ook gebruikt in andere scripts |
| $dag_nummer= | is | getEval | on | var n=${SCREENSHOT_DAGEN}; var i=(!today (D)/n); i%1*n; |
| save screenshot after | step | in folder | http://files/testResults/${RUNNING_PAGE_PATH}.${RUNNING_PAGE_NAME}/$dag_nummer |
******!
| script |
| show | screenshot location  |

'''Standaard webdriver timeout instellen'''
| script |
| set timeout to | ${WEBDRIVER_TIMEOUT} | seconds |
| ensure | do | open | on | ${LOCATION_BVHWEB} |
| ensure | do | open | on | ${LOCATION_BVHWEB} |
| bewaar schermprint met commentaar | BVH gestart |

'''Standaardvertraging instellen'''
| script |
| set step delay to | ${STEPDELAY} |
