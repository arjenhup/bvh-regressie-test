'''Opruimen zaakregistratie'''
| scenario | [Database Objects] Opruimen zaakregistratie met titel | Titel |
| start | Db Slim Update Query | exec p_test_delete_zaakregistratie_by_titel @Titel |
| show | rowsUpdated; |
| check | rowsUpdated; | <= 1 |

'''Tellen  zaakregistraties'''
| scenario | [Database Objects] Tellen zaakregistraties met titel | Titel | en verwacht aantal | Aantal |
| note | onderstaand werkt niet omdat de query niet correct is. Ik (EdwinR) kan de juiste niet even snel bedenken maar het principe is hopelijk duidelijk |
#| start | Db Slim Select Query | select count(*) from zaakregistratie where titel=@Titel |
#| check | dataByColumnIndexAndRowIndex; | 0 | 0 | @Aantal |
